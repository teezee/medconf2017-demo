# Medconf 2017 Hacking Demonstration

Tools und Informationen zum Demonstrieren verschiedener Hacking Methoden.
Entwickelt und getestet mit Kali-Linux-Light 2017.1 unter VirtualBox.

Um die Tools zu verwenden, muss in Kali Linux git installiert werden und das Repo ausgecheckt werden.

	apt update && apt install -y git
	cd ~/
	git clone https://git.uni-konstanz.de/thomas-zink/medconf2017.git

Die verschiedenen Verzeichnisse enthalten die Unterverzeichnisse `demo` und `presentation`.
Unter `demo` sind die Tools fuer eine Live-Demo enthalten.
Unter `presentation` befinden sich Informationen fuer die Präsentation.

Die folgenden Abschnitte beschreiben die Inhalte genauer.


## Brute Force SSH Password Cracking

### Demo

Demonstriert einen brute force Angriff auf ssh mit dem Programm `hydra`.

Zuerst muss das System vorbereitet werden:
- Änderung der sshd_config
- Starten des ssh Service
- Installieren von hydra

Hierzu einfach einmalig das folgende script ausführen (danach ist alles dauerhaft installiert und konfiguriert):

	setup_system.sh

Danach kann der Angriff demonstriert werden mit folgendem Aufruf.

	ssh_brute_force.sh

### Presentation

`hydra` verwendet einen sog. `dictionary attack` zum Erraten von Passwoertern.
Hierbei wird einfach so schnell wie moeglich (oder gewuenscht) eine Liste von Passwoertern (hier in der Datei `passwords.txt` durchgegangen und probiert, welches funktioniert.

Es gibt eine Menge kostenlos verfuegbarer Passwortlisten im Netz.
Siehe [SecLists](https://github.com/danielmiessler/SecLists).
Diese enthalten default Passwoerter, haeufig vorkommende Passwoerter, oder aber auch durch hacker gestohlene oder geleakte Passwoerter.

Im Internet erreichbare Rechner sind praktisch permanent unter brute force Angriffen, ausgesetzt.
Eine Statistik laesst sich durch Evaluation der authentication logs erstellen.

Hierzu werden zwei Scripte bereit gestellt.

- `auth_errors.sh`
    Shell script, dass aus einem auth.log alle fehlgeschlagenen Authentisierungsversuche extrahiert.
    Es nimmt als Standard das Log `/var/log/auth.log`, man kann aber auch ein Log als Argument uebergeben.

- `auth_errors.R`
    Rscript, dass die Ergebnisse aus `auth_errors.sh` sortiert, die Haeufigkeit berechnet und einen Plot anfertigt.
    Als Argument wird die Ausgabe von `auth_errors.sh` uebergeben. Die Scripte koennen auch per `pipe` miteinander verwendet werden.


## Find Password in Symbols

### Demo

Demonstriert wird, wie man in einem _binary_ durch simple Befehle ein Passwort anzeigen kann, dass mitkompiliert wurde und sich daher in der sog. _symboltable_ befindet.

Das binary wird kompiliert mit

    make

Dies erstellt die ausführbahre Datei `pwsymbols`.
Hiermit kann die normale Funktion demonstriert werden.
Das Passwort lautet: **PASSWORD**

Zum Anzeigen des Passwords kann man das script `show_password.sh` ausführen.

    ./show_password.sh

Es zeigt die _symboltable_ von `pwdb` und färbt das PASSWORD ein.

### Presentation

Werden Passwörter als Strings im Quellcode definiert, werden sie mitkompiliert und landen in der `symboltable`, welche alle Strings, Konstanten, Pointer etc beinhaltet.
Hat man Zugriff of das kompilierte Programm, ist es somit ein leichtes, das Password aus der Symboltabelle herauszulesen.

Neben dem Befehl `strings` gibt es noch weitere nützliche Befehle zum Untersuchen von _binaries_ (oder _object files_).

- Informationen anzeigen, wie header (-x), symbols (-s):

        objdump -s -x <binary>

- Mit dem hexeditor das binary inspizieren (auch hier findet man die Symbole)

        hexdump -C <binary>

- Anzeigen der im _binary_ verlinkten Bibliotheken

        ldd <binary>


## Find Password in Memory

#### Demo

Demonstriert das Auslesen des Passwortes aus dem Speicher eines gerade laufenden Programms.
Das Program `gdb` muss installiert sein.
Hierfür einfach einmalig `setup_system.sh` ausführen.

Zum Demonstrieren einfach das Script `show_passwort.sh` ausführen.

    ./show_password.sh

Es startet `gdb` im _batchmode_, setzt den _breakpoint_, lässt das Progamm laufen, und gibt anschliessend den Inhalt des Speichers zurück, an dem man das Passwort finden kann (PASSWORD).

#### Presentation

Wird das Passwort durch eine Funktion bspw. aus einer Datenbank geladen, so landet es im Speicherbereich des Programms (im _heap_).
Kennt man die Stelle, an der das Passwort geladen wird (bspw. nach Aufruf der Funktion `getPassword`, was man durch Inspektion der _symboltable_ rausfinden kann), so findet man die Adresse des Speichers nach Rückgabe der Funktion im Register _RAX_.
Diesen muss man sich nur anzeigen lassen und sieht das Passwort.

## Manipulate Authentication

## Buffer Overflow

### Demo overflow

Das Programm `overflow` demonstriert, wie man durch einen Pufferüberlauf erweiterte Rechte erlangen kann.
Kompilieren mit

    make

Das Programm erwartet eine Passworteingabe.
Gibt man mehr als 15 Zeichen ein, so erfolgt ein Pufferüberlauf und man kann den priviligierten Bereich ohne Authentifizierung betreten.

### Presentation overflow

Das Problem tritt auf da die Variable `int pass` hinter dem Puffer `char buff[15]` initialisiert, 
und zum kopieren in den Puffer die unsichere Funktion `strcmp` verwendet wird.
`strcmp` hat prueft nicht, ob die zu kopierenden Daten ueber den Zielspeicher hinausgehen.
Daher wird der Wert von `pass` ueberschrieben, wenn eine zu lange Eingabe erfolgt.
In diesem moment wird `pass != 0` und der Nutzer gilt in dieser Applikation als authentifiziert.

### Demo vuln

Beispiel von:

- [https://www.youtube.com/watch?v=eYrfWpkvMxA](https://www.youtube.com/watch?v=eYrfWpkvMxA)
- [https://gist.github.com/apolloclark/6cffb33f179cc9162d0a](https://gist.github.com/apolloclark/6cffb33f179cc9162d0a)

Soll funktionieren mit Kali 2016.1. Bisher nicht zum laufen gebracht.

### Presentation vuln
