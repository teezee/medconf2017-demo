#!/bin/bash
###
# Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

# allow root login
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config

# start ssh service
service ssh start

# configure ssh service to start at boot
systemctl enable ssh

# install hydra
apt update && apt install -y hydra
