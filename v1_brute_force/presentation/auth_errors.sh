#!/bin/bash
###
# Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

set -euf -o pipefail

usage() {
    echo "usage: $0 <path/to/log>";
	exit 0;
}

# search strings
invalid=": invalid user"
failed="Failed password"
maxauth="maximum authentication"

# set log file
LOG=/var/log/auth.log
if [ "$#" == 1 ] ; then
	LOG=$1;
fi

# check for invalid users, output "user #attempts"
#cat $LOG | grep "${invalid}" | cut -d " " -f 9 | sort | uniq | while read line; do echo -n $line" "; grep "${line}" $LOG | wc -l; done | sort -r -n -k 2 
cat $LOG | grep "${invalid}" | cut -d " " -f 9 

# check for failed password attempts, output "user #attempts"
#cat $LOG | grep "${failed}" | cut -d " " -f 11 | sort | uniq | while read line; do echo -n $line" "; grep "${line}" $LOG | wc -l; done | sort -r -n -k 2 
cat $LOG | grep "${failed}" | cut -d " " -f 11 

# check for maximum authentication attempts, output "user #attempts"
#cat $LOG | grep "${maxauth}" | grep -v -i "invalid user" | cut -d " " -f 12 | sort | uniq | while read line; do echo -n $line" "; grep "${line}" $LOG | wc -l; done | sort -r -n -k 2 
cat $LOG | grep "${maxauth}" | grep -v -i "invalid user" | cut -d " " -f 12 