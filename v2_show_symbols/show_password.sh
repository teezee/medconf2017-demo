#!/bin/bash

# build binary
make

# inspect binary and color the password
strings pwsymbols | grep --color -E '^|PASSWORD'
