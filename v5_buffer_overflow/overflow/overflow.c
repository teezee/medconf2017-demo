#include <stdio.h>
#include <string.h>

int main(void) {
    char buff[15];
    int pass = 0;

    printf("Enter the password : \n");
    gets(buff);

    if(strcmp(buff, "PASSWORD")) {
        printf ("Failure!\n");
    }
    else {
        printf ("Success!\n");
        pass = 1;
    }

    if(pass) {
        printf ("\nYou have been authenticated!\n");
    }

    return 0;
}
