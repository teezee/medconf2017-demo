#!/bin/bash

# install gdb if not yet available
command -v gdb >/dev/null 2>&1 || { apt update && apt install -y gdb }

# turn off ASLR
bash -c 'echo "kernel.randomize_va_space = 0" >> /etc/sysctl.conf'
sysctl -p

# unlimit shell ressources
ulimit -c unlimited

echo "ASLR disabled. You should restart the system"

